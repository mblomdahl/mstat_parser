# -*- coding: utf-8 -*-
#
# Title:   mstat_parser.JsonParser
# Author:  Mats Blomdahl
# Version: 2014-03-15

import logging
import pprint
import json
import datetime

from google.appengine.ext import ndb

import mstat_model.transactions as transaction_model

import mstat_generic_handler as generic_handler


_DEBUG = False


try:
    import depr_json_parser2 as depr_json_parser
    process_lf_json_api = depr_json_parser.DeprJsonParser.process_lf_json_api
    process_sf_json_api = depr_json_parser.DeprJsonParser.process_sf_json_api
except ImportError:
    process_lf_json_api = None
    process_sf_json_api = None


class JsonParser(generic_handler.CommonTaskQueueInterface):
    u"""Handlers responsible for performing JSON parsing, post-processing tasks and finalizing Datastore write ops.

    Security: All handlers are secured by App Engine's built-in authentication (`/mstat_parser/handlers.yaml` config).
    """

    # Interface: /backend/jsonparser/process_lf_json_api
    def process_lf_json_api(self):
        if process_lf_json_api:
            return process_lf_json_api(self)
        else:
            return False

    # Interface: /backend/jsonparser/process_sf_json_api
    def process_sf_json_api(self):
        if process_sf_json_api:
            return process_sf_json_api(self)
        else:
            return False

    # Interface: /backend/jsonparser/put_lf_raw_data_entry?opt_write=<true|false>&task_data=<json_obj>
    def put_lf_raw_data_entry(self):
        u"""Performs a Datastore create-or-update for a single ``LfRawDataEntry`` entity."""

        opt_write = self.request.get('opt_write', default_value=None)
        assert opt_write in ('true', 'false')
        opt_write = opt_write == 'true'

        task_data = self.request.get('task_data', default_value=None)
        assert task_data is not None
        task_data = json.loads(task_data)
        if _DEBUG:
            logging.debug('[JsonParser.put_lf_raw_data_entry] task_data: \'%s\'' % pprint.pformat(task_data))

        lf_entity_key = ndb.Key(transaction_model.LfRawDataEntry, task_data['uniknr'])

        lf_entity = lf_entity_key.get()

        futures = []

        if lf_entity is None:
            logging.info('[JsonParser.put_lf_raw_data_entry] Creating LF entity key ID \'%s\'.'
                         % lf_entity_key.id())

            lf_entity = transaction_model.LfRawDataEntry(
                key=lf_entity_key,
                **task_data
            )
            lf_entity.backfill()

            try:
                composite, future = lf_entity.to_transaction_composite(delegate_future_mgnt=True)
                futures.append(future)
            except AssertionError as e:
                logging.error(
                    '[JsonParser.put_lf_raw_data_entry] \'to_transaction_composite\' raised AssertionError: \'%s\''
                    % e.message)

            lf_entity.put(deadline=3600)

        else:
            if opt_write and task_data['aktTid'] == lf_entity.aktTid.isoformat():
                logging.info('[JsonParser.put_lf_raw_data_entry] Found matching key ID \'%s\' with unchanged '
                             '\'aktTid\' (%s). Aborting...' % (lf_entity.key.id(), task_data['aktTid']))
                return
            else:
                logging.info('[JsonParser.put_lf_raw_data_entry] Re-using entity key ID \'%s\'.' % lf_entity.key.id())

            lf_entity.populate(**task_data)
            lf_entity.backfill()

            try:
                composite, future = lf_entity.to_transaction_composite(opt_put=opt_write, delegate_future_mgnt=True)
                if future:
                    futures.append(future)
            except AssertionError as e:
                logging.error(
                    '[JsonParser.put_lf_raw_data_entry] \'to_transaction_composite\' raised AssertionError: \'%s\''
                    % e.message)

            if opt_write:
                lf_entity.opt_put(deadline=3600)
            else:
                lf_entity.put(deadline=3600)

        ndb.Future.wait_all(futures)
        for future in futures:
            future.get_result()  # Never forget!

    # Interface: /backend/jsonparser/put_sf_raw_data_entry?opt_write=<true|false>&task_data=<json_obj>
    def put_sf_raw_data_entry(self):
        u"""Performs a Datastore create-or-update for a single ``SfRawDataEntry`` entity."""

        opt_write = self.request.get('opt_write', default_value=None)
        assert opt_write in ('true', 'false')
        opt_write = opt_write == 'true'

        task_data = self.request.get('task_data', default_value=None)
        assert task_data is not None
        task_data = json.loads(task_data)
        if _DEBUG:
            logging.debug('[JsonParser.put_sf_raw_data_entry] task_data: \'%s\'' % pprint.pformat(task_data))

        sf_entity_key = ndb.Key(transaction_model.SfRawDataEntry, task_data['uniknr'])

        sf_entity = sf_entity_key.get()

        futures = []

        if _DEBUG:
            logging.debug('[JsonParser.put_sf_raw_data_entry] sf_entity: \'%s\' / dir(sf_entity): \'%s\''
                          % (sf_entity, pprint.pformat(dir(sf_entity))))
            try:
                logging.debug('[JsonParser.put_sf_raw_data_entry] sf_entity.key: \'%s\', dir(sf_entity.key): \'%s\''
                              % (sf_entity.key, pprint.pformat(dir(sf_entity.key))))
            except AttributeError:
                logging.debug('[JsonParser.put_sf_raw_data_entry] sf_entity.key: None')

        if sf_entity is None:
            logging.info('[JsonParser.put_sf_raw_data_entry] Creating SF entity key ID \'%s\'.'
                         % sf_entity_key.id())

            sf_entity = transaction_model.SfRawDataEntry(
                key=sf_entity_key,
                **task_data
            )
            sf_entity.backfill()

            try:
                composite, future = sf_entity.to_transaction_composite(delegate_future_mgnt=True)
                futures.append(future)
            except AssertionError as e:
                logging.error(
                    '[JsonParser.put_sf_raw_data_entry] \'to_transaction_composite\' raised AssertionError: \'%s\''
                    % e.message)

            sf_entity.put(deadline=3600)

        else:
            if opt_write and task_data['aktTid'] == sf_entity.aktTid.isoformat():
                logging.info('[JsonParser.put_sf_raw_data_entry] Found matching key ID \'%s\' with unchanged '
                             '\'aktTid\' (%s). Aborting...' % (sf_entity.key.id(), task_data['aktTid']))
                return
            else:
                logging.info('[JsonParser.put_sf_raw_data_entry] Re-using entity key ID \'%s\'.' % sf_entity.key.id())

            sf_entity.populate(**task_data)
            sf_entity.backfill()

            try:
                composite, future = sf_entity.to_transaction_composite(opt_put=opt_write, delegate_future_mgnt=True)
                if future:
                    futures.append(future)
            except AssertionError as e:
                logging.error(
                    '[JsonParser.put_sf_raw_data_entry] \'to_transaction_composite\' raised AssertionError: \'%s\''
                    % e.message)

            if opt_write:
                sf_entity.opt_put(deadline=3600)
            else:
                sf_entity.put(deadline=3600)

        ndb.Future.wait_all(futures)
        for future in futures:
            future.get_result()  # Never forget!

    # Interface: /backend/jsonparser/put_capitex_raw_data_entry?opt_write=<true|false>&task_data=<json_obj>
    #    &origin_email_key_urlsafe=<ndb_key>
    def put_capitex_raw_data_entry(self):
        u"""Performs a Datastore create-or-update for a single ``CapitexRawDataEntry`` entity."""

        opt_write = self.request.get('opt_write', default_value=None)
        assert opt_write in ('true', 'false')

        task_data = self.request.get('task_data', default_value=None)
        assert task_data is not None
        task_data = json.loads(task_data)

        origin_email_key_urlsafe = self.request.get('origin_email_key_urlsafe', default_value=None)
        assert origin_email_key_urlsafe is not None

        if _DEBUG:
            logging.debug('[JsonParser.put_capitex_raw_data_entry] task_data: \'%s\'' % pprint.pformat(task_data))
        task_data['sys_origin_email_entry'] = ndb.Key(urlsafe=origin_email_key_urlsafe)

        capitex_entity_key = ndb.Key(transaction_model.CapitexRawDataEntry, task_data['ObjektID'])

        capitex_entity = capitex_entity_key.get()

        futures = []

        if _DEBUG:
            logging.debug('[JsonParser.put_capitex_raw_data_entry] capitex_entity: \'%s\' / dir(capitex_entity): \'%s\''
                          % (capitex_entity, pprint.pformat(dir(capitex_entity))))
            try:
                logging.debug(
                    '[JsonParser.put_capitex_raw_data_entry] capitex_entity.key: %r / dir(capitex_entity.key): \'%s\''
                    % (capitex_entity.key, pprint.pformat(dir(capitex_entity.key))))
            except AttributeError:
                logging.debug('[JsonParser.put_capitex_raw_data_entry] capitex_entity.key: None')

        if capitex_entity is None:
            logging.info('[JsonParser.put_capitex_raw_data_entry] Creating Capitex entity key ID \'%s\'.'
                         % capitex_entity_key.id())

            capitex_entity = transaction_model.CapitexRawDataEntry(
                key=capitex_entity_key,
                **task_data
            )
            capitex_entity.backfill()

            try:
                composite, future = capitex_entity.to_transaction_composite(delegate_future_mgnt=True)
                futures.append(future)
            except AssertionError as e:
                logging.error(
                    '[JsonParser.put_capitex_raw_data_entry] \'to_transaction_composite\' raised AssertionError: \'%s\''
                    % e.message)

            capitex_entity.put()

        else:
            if opt_write and task_data['sys_last_submitted'] == capitex_entity.sys_last_submitted.isoformat():
                logging.info(
                    '[JsonParser.put_capitex_raw_data_entry] Found matching key ID \'%s\' with unchanged \'sys_last'
                    '_submitted\' (%s). Aborting...' % (capitex_entity.key.id(), task_data['sys_last_submitted']))
                return
            else:
                logging.info(
                    '[JsonParser.put_capitex_raw_data_entry] Re-using entity key ID \'%s\'.' % capitex_entity.key.id())

            capitex_entity.populate(**task_data)
            capitex_entity.backfill()

            try:
                composite, future = capitex_entity.to_transaction_composite(opt_put=opt_write,
                                                                            delegate_future_mgnt=True)
                if future:
                    futures.append(future)
            except AssertionError as e:
                logging.error(
                    '[JsonParser.put_capitex_raw_data_entry] \'to_transaction_composite\' raised AssertionError: \'%s\''
                    % e.message)

            if opt_write:
                capitex_entity.opt_put(deadline=3600)
            else:
                capitex_entity.put(deadline=3600)

        ndb.Future.wait_all(futures)
        for future in futures:
            future.get_result()  # Never forget!

    # Interface: /backend/jsonparser/put_sfd_raw_data_entry?opt_write=<true|false>&task_data=<json_obj>
    def put_sfd_raw_data_entry(self):
        u"""Performs a Datastore create-or-update for a single ``SfdRawDataEntry`` entity."""

        opt_write = self.request.get('opt_write', default_value=None)
        assert opt_write in ('true', 'false')
        opt_write = opt_write == 'true'

        task_data = self.request.get('task_data', default_value=None)
        assert task_data is not None
        task_data = json.loads(task_data)
        if _DEBUG:
            logging.debug('[JsonParser.put_sfd_raw_data_entry] task_data: \'%s\'' % pprint.pformat(task_data))

        sfd_entity_key = ndb.Key(transaction_model.SfdRawDataEntry, task_data['Uniknr'])

        sfd_entity = sfd_entity_key.get()

        futures = []

        if _DEBUG:
            logging.debug('[JsonParser.put_sfd_raw_data_entry] sfd_entity: \'%s\' / dir(sfd_entity): \'%s\''
                          % (sfd_entity, pprint.pformat(dir(sfd_entity))))
            try:
                logging.debug('[JsonParser.put_sfd_raw_data_entry] sfd_entity.key: \'%s\', dir(sfd_entity.key): \'%s\''
                              % (sfd_entity.key, pprint.pformat(dir(sfd_entity.key))))
            except AttributeError:
                logging.debug('[JsonParser.put_sfd_raw_data_entry] sfd_entity.key: None')

        if sfd_entity is None:
            logging.info('[JsonParser.put_sfd_raw_data_entry] Creating SFD entity key ID \'%s\'.' % sfd_entity_key.id())

            sfd_entity = transaction_model.SfdRawDataEntry(
                key=sfd_entity_key,
                **task_data
            )

            sfd_entity.backfill()

            try:
                composite, future = sfd_entity.to_transaction_composite(delegate_future_mgnt=True)
                futures.append(future)
            except AssertionError as e:
                logging.error(
                    '[JsonParser.put_sfd_raw_data_entry] \'to_transaction_composite\' raised AssertionError: \'%s\''
                    % e.message)

            sfd_entity.put(deadline=3600)

        else:
            if opt_write and task_data['AktTid'] == sfd_entity.AktTid.isoformat():
                logging.info('[JsonParser.put_sfd_raw_data_entry] Found matching key ID \'%s\' with unchanged '
                             '\'AktTid\' (%s). Aborting...' % (sfd_entity.key.id(), task_data['AktTid']))
                return
            else:
                logging.info('[JsonParser.put_sfd_raw_data_entry] Re-using entity key ID \'%s\'.' % sfd_entity.key.id())

            sfd_entity.populate(**task_data)

            sfd_entity.backfill()

            try:
                composite, future = sfd_entity.to_transaction_composite(opt_put=opt_write, delegate_future_mgnt=True)
                if future:
                    futures.append(future)
            except AssertionError as e:
                logging.error(
                    '[JsonParser.put_sfd_raw_data_entry] \'to_transaction_composite\' raised AssertionError: \'%s\''
                    % e.message)

            if opt_write:
                sfd_entity.opt_put(deadline=3600)
            else:
                sfd_entity.put(deadline=3600)

        ndb.Future.wait_all(futures)
        for future in futures:
            future.get_result()  # Never forget!

    # Interface: /backend/jsonparser/put_fmf_raw_data_entry?opt_write=<true|false>&task_data=<json_obj>
    def put_fmf_raw_data_entry(self):
        u"""Performs a Datastore create-or-update for a single ``SfdFmfRawDataEntry`` entity."""

        opt_write = self.request.get('opt_write', default_value=None)
        assert opt_write in ('true', 'false')
        opt_write = opt_write == 'true'

        task_data = self.request.get('task_data', default_value=None)
        assert task_data is not None
        task_data = json.loads(task_data)
        if _DEBUG:
            logging.debug('[JsonParser.put_fmf_raw_data_entry] task_data: \'%s\'' % pprint.pformat(task_data))

        fmf_entity_key = ndb.Key(transaction_model.SfdFmfRawDataEntry, task_data['Uniknr'])

        fmf_entity = fmf_entity_key.get()

        if _DEBUG:
            logging.debug('[JsonParser.put_fmf_raw_data_entry] fmf_entity: \'%s\' / dir(fmf_entity): \'%s\''
                          % (fmf_entity, pprint.pformat(dir(fmf_entity))))
            try:
                logging.debug('[JsonParser.put_fmf_raw_data_entry] fmf_entity.key: \'%s\', dir(fmf_entity.key): \'%s\''
                              % (fmf_entity.key, pprint.pformat(dir(fmf_entity.key))))
            except AttributeError:
                logging.debug('[JsonParser.put_fmf_raw_data_entry] fmf_entity.key: None')

        if fmf_entity is None:
            logging.info('[JsonParser.put_fmf_raw_data_entry] Creating FMF entity key ID \'%s\'.' % fmf_entity_key.id())

            fmf_entity = transaction_model.SfdFmfRawDataEntry(
                key=fmf_entity_key,
                **task_data
            )

            fmf_entity.backfill()

            fmf_entity.put(deadline=3600)

        else:
            if opt_write and task_data['AktTid'] == fmf_entity.AktTid.isoformat():
                logging.info('[JsonParser.put_fmf_raw_data_entry] Found matching key ID \'%s\' with unchanged '
                             '\'AktTid\' (%s). Aborting...' % (fmf_entity.key.id(), task_data['AktTid']))
                return
            else:
                logging.info('[JsonParser.put_fmf_raw_data_entry] Re-using entity key ID \'%s\'.' % fmf_entity.key.id())

            fmf_entity.populate(**task_data)

            fmf_entity.backfill()

            if opt_write:
                fmf_entity.opt_put(deadline=3600)
            else:
                fmf_entity.put(deadline=3600)

    # Interface: /backend/jsonparser/put_scb_raw_data_entry?opt_write=<true|false>&task_data=<json_obj>
    def put_scb_raw_data_entry(self):
        u"""Performs a Datastore create-or-update for a single ``ScbRawDataEntry`` entity."""

        opt_write = self.request.get('opt_write', default_value=None)
        assert opt_write in ('true', 'false')
        opt_write = opt_write == 'true'

        task_data = self.request.get('task_data', default_value=None)
        assert task_data is not None
        task_data = json.loads(task_data)
        if _DEBUG:
            logging.debug('[JsonParser.put_scb_raw_data_entry] task_data: \'%s\'' % pprint.pformat(task_data))

        scb_entity_key = ndb.Key(transaction_model.ScbRawDataEntry, task_data['ObjektId'])

        scb_entity = scb_entity_key.get()

        futures = []

        if scb_entity is None:
            logging.info('[JsonParser.put_scb_raw_data_entry] Creating SCB entity key ID \'%s\'.' % scb_entity_key.id())

            scb_entity = transaction_model.ScbRawDataEntry(
                key=scb_entity_key,
                **task_data
            )

            scb_entity.backfill()

            try:
                composite, future = scb_entity.to_transaction_composite(delegate_future_mgnt=True)
                futures.append(future)
            except AssertionError as e:
                logging.error(
                    '[JsonParser.put_scb_raw_data_entry] \'to_transaction_composite\' raised AssertionError: \'%s\''
                    % e.message)

            scb_entity.put(deadline=3600)

        else:
            transfer_date = datetime.datetime.strptime(task_data['Overforingsdatum'].split('.')[0],
                                                       '%d%b%Y:%H:%M:%S').isoformat()
            if opt_write and transfer_date == scb_entity.Overforingsdatum.isoformat():
                logging.info('[JsonParser.put_scb_raw_data_entry] Found matching key ID \'%s\' with unchanged \'Over'
                             'foringsdatum\' (%s). Aborting...' % (scb_entity.key.id(), transfer_date))
                return
            else:
                logging.info('[JsonParser.put_scb_raw_data_entry] Re-using entity key ID \'%s\' for new \'Overforings'
                             'datum\' (%s)...' % (scb_entity.key.id(), transfer_date))

            scb_entity.populate(**task_data)

            scb_entity.backfill()

            try:
                composite, future = scb_entity.to_transaction_composite(opt_put=opt_write, delegate_future_mgnt=True)
                if future:
                    futures.append(future)
            except AssertionError as e:
                logging.error(
                    '[JsonParser.put_scb_raw_data_entry] \'to_transaction_composite\' raised AssertionError: \'%s\''
                    % e.message)

            if opt_write:
                scb_entity.opt_put(deadline=3600)
            else:
                scb_entity.put(deadline=3600)

        ndb.Future.wait_all(futures)
        for future in futures:
            future.get_result()  # Never forget!




