# -*- coding: utf-8 -*-
#
# Title:   mstat_parser.module.app
# Author:  Mats Blomdahl
# Version: 2013-09-04

from __future__ import absolute_import

import logging
import webapp2

import ecore


CONFIG = {}


def _handle_400(request, response, exception):
    logging.exception(exception)
    response.write(
        'The server could not comply with the request since it is either malformed or otherwise incorrect.')
    response.set_status(400)


def _handle_404(request, response, exception):
    logging.exception(exception)
    response.write('Oops! I could swear this page was here!')
    response.set_status(404)


def _handle_500(request, response, exception):
    logging.exception(exception)
    response.write('A server error occurred!<br><br>Details:')
    response.set_status(500)


app = webapp2.WSGIApplication(routes=[
    # Handler for starting the backend service.
    webapp2.Route('/_ah/start',
                  handler='ecore.handler.BackendStartup',
                  methods=['GET', 'POST']),

    # Handlers responsible for email parsing and post-processing tasks.
    webapp2.Route('/backend/emailparser/<process:\w+>',
                  handler='mstat_parser.EmailParser:handle',
                  methods=['GET', 'POST']),

    # Handlers responsible for CSV parsing and post-processing tasks.
    webapp2.Route('/backend/csvparser/<process:\w+>',
                  handler='mstat_parser.CsvParser:handle',
                  methods=['GET', 'POST']),

    # Handlers responsible for JSON parsing and post-processing tasks.
    webapp2.Route('/backend/jsonparser/<process:\w+>',
                  handler='mstat_parser.JsonParser:handle',
                  methods=['GET', 'POST'])

], config=CONFIG, debug=True)

app.error_handlers[400] = _handle_400
app.error_handlers[404] = _handle_404
app.error_handlers[500] = _handle_500


def main():
    u"""Starts the webapp2 framework."""

    app.run()


if __name__ == '__main__':
    main()

