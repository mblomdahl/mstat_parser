# _mstat\_parser_

The App Engine _parser_ module takes care of all non-trivial Datastore write operations. E.g.

* best-effort parsing and storage of unreliable supplier API output and
* best-effort parsing and storage of inbound emails (including any attached CSV data).

All input is supplied by the [_mstat\_worker_](https://bitbucket.org/mblomdahl/mstat_worker) module.


Base paths overview:

* `/backend/jsonparser/<cmd>` – ...
* `/backend/emailparser/<cmd>` – ...


## 1. Prerequisites

Software:

* [Google App Engine Python SDK 1.9.2](https://developers.google.com/appengine/downloads)
* [Python 2.7.6](http://python.org/download/)
* [ecore 1.1](https://bitbucket.org/mblomdahl-ondemand/ecore)[^private]
* [mstat 1.06](https://bitbucket.org/mblomdahl-ondemand/mstat)[^private]

[^private]: Currently *not* publicly accessible.


## 2. Configuration

Locate `mstat` dev root and run

    $ git clone git@bitbucket.org:mblomdahl/mstat_parser.git


### 2.1. Module YAML Config

Add a `mstat_parser.yaml` configuration to the `mstat` root and set runtime environment to threadsafe Python 2.7 and
platform API version 1:

```
#!yaml
module: parser
api_version: 1
threadsafe: true
runtime: python27
```


Include the module's handlers/builtins configs:

```
#!yaml
includes:
- mstat_parser/handlers.yaml
- mstat_parser/builtins.yaml
```


Include required third-party libraries:

```
#!yaml
libraries:
  # Required for all request handling.
- name: webapp2
  version: latest

  # Required templating engine for the `ecore` library.
- name: jinja2
  version: latest
```


It's also highly recommended to exclude any unnecessary root resources from the module config, e.g.

```
#!yaml
skip_files:
  # Misc Application Logic
- ^(.*/)?mstat_worker/.*
- ^(.*/)?mstat_static_ui/.*
- ^(.*/)?mstat_front-end_api/.*
- ^(.*/)?mstat_static_resources/.*
- ^(.*/)?mstat_tce_userspace_stats/.*
- ^(.*/)?mstat_default/.*

  # Misc Resources
- ^(.*/)?playground/.*
- ^(.*/)?resources/.*
- ^(.*/)?templates/.*
- ^(.*/)?samples/.*

  # Configuration and Backup Files
- ^(.*/)?app\.yaml
- ^(.*/)?dispatch\.yaml
- ^(.*/)?mapreduce\.yaml
- ^(.*/)?index\.yaml
- ^(.*/)?cron\.yaml
- ^(.*/)?.*~
- ^(.*/)?.*\.py[co]
- ^(.*/)?.*/RCS/.*
- ^(.*/)?\..*
- ^(.*/)?.*\.bak$
```


### 2.2. Dispatch YAML Config

Add the following route to the `dispatch.yaml` configuration in the `mstat` root:

```
#!yaml
dispatch:
  # Route all invocations within the `mstat_parser` base paths to the ``parser`` module.
- url: "*/backend/jsonparser/*"
  module: parser
- url: "*/backend/emailparser/*"
  module: parser
```


## 3. Deployment

The module is deployed just like any other GAE module:

    $ appcfg.py --oauth2 update mstat_parser.yaml


## 4. Best Practices / Design Goals

* No errors should pass silently, ever.
    * All jobs are run through the App Engine TaskQueue – this assures any broken logic will accumulate as failing
    tasks in the application overview pages.
    * All jobs submitted to the TaskQueue must be stateless – this assures that the tasks will be able to complete once
    the dev team deploys a bug fix.
* Split any multi-step routines into single-step web hooks wherever possible.
* Beware of fork bombs (may cause several thousand euros in damages).
* Always strive to find a "functional programming approach" to data processing within GAE.


## 5. Code Style

The project code style primarily relies on [PEP-8](http://google-styleguide.googlecode.com/svn/trunk/pyguide.html) and
[Google Python Style Guide](http://google-styleguide.googlecode.com/svn/trunk/pyguide.html).

Notable exceptions:

* Line-width: 120 ch.
* Imports convention

    No:

        from mstat_model.transactions import SfdRawDataEntry
        SfdRawDataEntry.get_default_sort_property()

    No:

        import mstat_model.transactions
        mstat_model.transactions.SfdRawDataEntry.get_default_sort_property()

    Yes:

        import mstat_model.transactions as transaction_model
        transaction_model.SfdRawDataEntry.get_default_sort_property()


Encoding: UTF-8

Line separator: Always LF (\n), never CRLF

Indentation: Always 4-space indents, never tabs (\t)


## 6. Known Issues

Many:

* The `emailparser` component is *really* broken.


## 7. Changelog


### Version 1.06

Changes:

* Began writing on the module `README.md`. Yey!
* ...


### Version 1.05 [draft]

Changes:

* Deprecated the `xmlparser` component.
* Deprecated the `csvparser` component.
* Refactored all direct API consumption tasks to the [_mstat\_worker_](https://bitbucket.org/mblomdahl/mstat_worker)
  module.
* Added the refactored `jsonparser` component.


### Version 1.04 [draft]

Changes:

* Implemented `opt_write` controls for all Datastore writes.


## 8. Contact

For general inquiries, security issues and feature requests, contact lead developer
[Mats Blomdahl](mailto:mats.blomdahl@gmail.com).
