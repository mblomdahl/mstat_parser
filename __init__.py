# -*- coding: utf-8 -*-
#
# Title:   mstat_parser
# Author:  Mats Blomdahl
# Version: 2013-09-04

"""mstat_parser module

Usage:
    Invoke URLs at ``/backend/<json|csv|email>parser/<cmd>`` using the GAE TaskQueue or Cron Service.
"""

try:
    from email_client import EmailClient
except ImportError:
    pass

try:
    from csv_parser import CsvParser
except ImportError:
    pass

try:
    from email_parser import EmailParser
except ImportError:
    pass

try:
    from json_parser import JsonParser
except ImportError:
    pass

